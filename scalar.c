#include <stdio.h>

const int n = 5;
const int c [] = {1,4,6,8,9};
const int d [] = {7,9,1,4,8};

double scalar_production(const int a[], const int b[], const int size){
    double result = 0;
    int i;
    
    for (i = 0; i < size; i++)
            result = result + (a[i])*(b[i]);

    return result;
}

int main(){

    double production = scalar_production(c, d, n);
    printf("%g", production);
    return 0;
}
