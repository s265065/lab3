#include <stdio.h>

int is_prime(unsigned long n){
    unsigned long i;
    for(i = 2; i*i < n; i++){
        if(n%i == 0){
            return 0;
        }
    }
    return 1;
}

int main(void){
    int result;
    unsigned long num;
    int check;
    
    printf("input number to check: ");
    check = scanf("%lu",&num);
    if (check != 1)
        return 1;

    result = is_prime(num);
    result?printf("yes\n"):printf("no\n");
    return 0;
}
