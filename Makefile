all: build

build: scalar.c primes.c
	gcc -o scalar -ansi -pedantic -Wall -Werror scalar.c
	gcc -o primes -ansi -pedantic -Wall -Werror primes.c

clean:
	rm -f scalar
	rm -f primes

